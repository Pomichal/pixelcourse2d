using System.Collections; using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;
using Main.Behaviours;
using Platformer.Managers;

namespace Platformer.Behaviours
{
    public class PlayerBehaviour : GenericPlayer
    {
        public float jumpForce;
	public float playerSpeed;
        public float groundCheckDist = 0.6f;

        public UnityEvent<Vector3Int, TileBase> onCoinCollected = new UnityEvent<Vector3Int, TileBase>();

        private Tilemap collectibleTilemap;
        private int collectedCoins;

        public void Init(Tilemap tilemap)
        {
            collectibleTilemap = tilemap;
        }

        void Update()
        {
            var hor = Input.GetAxis("Horizontal") * playerSpeed * 100 * Time.deltaTime;
            if(hor != 0)
            {
                rb.velocity = new Vector2(hor, rb.velocity.y);
            }
            if(Input.GetKeyDown(KeyCode.Space) && OnGround())
            {
                Jump();
            }
        }

        void Jump()
        {
            rb.velocity += new Vector2(0, jumpForce);
        }

        bool OnGround()
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, distance: groundCheckDist, layerMask: LayerMask.GetMask("Ground"));
            Debug.DrawRay(transform.position, Vector2.down * groundCheckDist);
            return hit.collider != null && hit.collider.gameObject.CompareTag("Ground");
        }

        protected override void OnTriggerEnter2D(Collider2D other)
        {
            if(other.CompareTag("Coin"))
            {
                if(collectibleTilemap != null)
                {
                    Vector3 contactPoint = other.ClosestPoint(transform.position);
                    Vector2 point = contactPoint + (contactPoint - transform.position).normalized * 0.1f;
                    Vector3Int tilePoint = collectibleTilemap.WorldToCell(point);
                    onCoinCollected.Invoke(tilePoint, collectibleTilemap.GetTile(tilePoint));
                    collectibleTilemap.SetTile(tilePoint, null);
                    collectedCoins++;
                    if(collectedCoins >= ((PlatformerLevelManager)App.levelManager).coinsToCollect)
                    {
                        onFinish.Invoke(true);
                    }
                }
            }
        }

        public override void Reset(Vector3 pos)
        {
            base.Reset(pos);
            collectedCoins = 0;
        }
    }
}
