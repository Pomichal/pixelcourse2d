using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Main.Managers;
using Platformer.Behaviours;
using UI;
using Commands;

namespace Platformer.Managers
{
    public class PlatformerLevelManager : LevelManager
    {
        public int coinsToCollect;
        public Tilemap collectibles;

        private Dictionary<Vector3Int, TileBase> coinRestartPosition;

        protected override void FirstTimeSetup()
        {
            coinRestartPosition = new Dictionary<Vector3Int, TileBase>();
            ((PlayerBehaviour)player).onCoinCollected.AddListener(CollectCoin);
            base.FirstTimeSetup();
            ((PlayerBehaviour)player).Init(collectibles);
        }

        public override void SetupScene()
        {
            foreach(var pos in coinRestartPosition)
            {
                collectibles.SetTile(pos.Key, pos.Value);

            }
            coinRestartPosition = new Dictionary<Vector3Int, TileBase>();
            base.SetupScene();
        }

        public override void Finish(bool win)
        {
            base.Finish(win);
            Dictionary<string, object> par = new Dictionary<string, object>
            {
                {"win", win},
                {"time", Time.time - timeSinceStart}
            };
            new ShowScreenCommand<GameOverScreen>(par).Execute();
        }

        private void CollectCoin(Vector3Int pos, TileBase tile)
        {
            coinRestartPosition.Add(pos, tile);
        }
    }
}
