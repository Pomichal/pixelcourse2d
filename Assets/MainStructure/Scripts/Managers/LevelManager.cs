using UnityEngine;
using Main.Behaviours;

namespace Main.Managers
{
    public class LevelManager : MonoBehaviour
    {

        public Transform startPosition;
        public GenericPlayer player;
        protected float timeSinceStart;
        protected bool started;

        public bool IsStarted{ get { return started;}}

        protected virtual void Start()
        {
            started = false;
            App.levelManager = this;
            FirstTimeSetup();
        }

        protected virtual void FirstTimeSetup()
        {
            player.onFinish.AddListener(Finish);
            SetupScene();
        }

        protected virtual void StartGame()
        {
            timeSinceStart = Time.time;
            player.gameObject.SetActive(true);
            player.Reset(startPosition.position);
            started = true;
        }

        protected virtual void Update()
        {
            if(Input.GetKeyDown(KeyCode.R))
            {
                SetupScene();
            }
            if(!started && Input.GetKeyDown(KeyCode.Space))
            {
                StartGame();
            }
        }

        public virtual void Finish(bool win)
        {
            started = false;
            player.gameObject.SetActive(false);
        }

        public virtual void SetupScene()
        {
            started = false;
            player.gameObject.SetActive(false);
            player.Reset(startPosition.position);
        }

        public float GetTimeSinceStart()
        {
            return started ? timeSinceStart : 0;
        }
    }
}
