﻿using Main.Managers;
using UnityEngine.Events;

public class App
{
    public static UnityEvent onLoadingProgressed = new UnityEvent();

    public static ScreenManager screenManager;
    public static GameManager gameManager;
    public static DataManager dataManager;
    public static SceneLoader sceneLoader;
    public static LevelManager levelManager;

    public static int verboseLevel;    // 0 - no logs ... 3 - log everything
}
