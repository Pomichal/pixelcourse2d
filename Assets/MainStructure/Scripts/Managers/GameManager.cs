﻿using Commands;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Main.Managers
{
    public class GameManager : MonoBehaviour
    {
        public int verboseLevel;

        void Start()
        {
            App.verboseLevel = verboseLevel;
            App.gameManager = this;
            App.dataManager = new DataManager();
            App.sceneLoader.LoadScene("UIScene", 1, new ShowScreenCommand<MenuScreen>());
        }

        public void StartGame(string levelName, ICommand ShowCommand)
        {
            App.sceneLoader.LoadScene(levelName, 1, ShowCommand, setAsActive: true);
        }

        public void ReturnToMenu()
        {
            App.sceneLoader.UnLoadScene(SceneManager.GetActiveScene().name, 1, new ShowScreenCommand<MenuScreen>());
        }
    }
}
