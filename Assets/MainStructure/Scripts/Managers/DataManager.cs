﻿using System.Collections.Generic;
using Commands;
using UI;

namespace Main.Managers
{
    public class DataManager
    {
        public Dictionary<int, string> gameNames = new Dictionary<int, string>
        {
            {0, "Platformer"},
        };

        public static Dictionary<int, ICommand> screenCommands = new Dictionary<int, ICommand>
        {
            {0, new ShowScreenCommand<CountdownInGameScreen>()},
        };

        public DataManager()
        {
        }

        public string GetSceneName(int id)
        {
            return gameNames[id];
        }

        public ICommand GetSceneCommand(int id)
        {
            return screenCommands[id];
        }
    }
}
