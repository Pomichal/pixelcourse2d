﻿using System.Collections.Generic;

namespace Commands
{
    public class ShowScreenCommand<T> : ICommand
    {
        Dictionary<string, object> parameter;

        public ShowScreenCommand(Dictionary<string, object> parameter=null)
        {
            this.parameter = parameter;
        }

        public void Execute()
        {
            if(parameter == null)
            {
                App.screenManager?.Show<T>();
            }
            else
            {
                App.screenManager?.Show<T>(parameter);
            }
        }
    }
}
