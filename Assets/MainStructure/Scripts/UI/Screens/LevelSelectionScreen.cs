using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace UI
{
    public class LevelSelectionScreen : ScreenBase
    {

        public Transform contentFitter;
        public LevelSelectionButton buttonPrefab;

        public override void Show(Dictionary<string, object> parameter=null)
        {
            base.Show(parameter);
            foreach (Transform item in contentFitter)
            {
                Destroy(item.gameObject);
            }
            int gameId = (int)parameter["gameId"];
            string[] files = Directory.GetFiles(Application.dataPath + "/Scenes/Games/" + App.dataManager.GetSceneName(gameId));
            foreach(var file in files)
            {
                var splitted = Path.GetFileName(file).Split('.');
                if(splitted[splitted.Length -1] != "meta")
                {
                    var button = Instantiate(buttonPrefab, contentFitter);
                    button.Init(splitted[0], gameId);
                }
            }

        }
    }
}
