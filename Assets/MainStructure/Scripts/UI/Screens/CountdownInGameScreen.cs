using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace UI
{
    public class CountdownInGameScreen : InGameScreen
    {
        public TextMeshProUGUI timeText;

        void Update()
        {
            if(App.levelManager != null)
            {
                float time = App.levelManager.GetTimeSinceStart();
                timeText.text = time != 0 ? (Time.time - App.levelManager.GetTimeSinceStart()).ToString() : "Press space to start";
            }
        }

        public void Restart()
        {
            App.levelManager?.SetupScene();
        }
    }
}
