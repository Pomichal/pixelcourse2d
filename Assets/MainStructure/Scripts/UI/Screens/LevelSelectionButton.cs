using System.Collections;
using System.Collections.Generic;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectionButton : MonoBehaviour
{
    public TextMeshProUGUI text;
    public string sceneName;

    private int gameId;

    public void Init(string name, int gameId)
    {
        sceneName = name;
        text.text = name;
        this.gameId = gameId;
        GetComponent<Button>().onClick.AddListener(LoadLevel);
    }

    private void LoadLevel()
    {
        App.gameManager.StartGame(sceneName, App.dataManager.GetSceneCommand(gameId));
        App.screenManager.Hide<GamesScreen>();
        App.screenManager.Hide<LevelSelectionScreen>();
    }
}
