﻿using Commands;

namespace UI
{
    public class MenuScreen : ScreenBase
    {

        public override void Show()
        {
            base.Show();
        }

        public override void Hide()
        {
            base.Hide();
        }

        public void StartButtonClicked()
        {
            new ShowScreenCommand<GamesScreen>().Execute();
            Hide();
        }
    }
}
