using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace UI
{
    public class GameOverScreen : ScreenBase
    {

        public TextMeshProUGUI resultText;
        public TextMeshProUGUI timeText;

        public override void Show(Dictionary<string, object> parameter=null)
        {
            base.Show(parameter);
            Logger.Log("gameover");
            bool win = (bool)parameter["win"];
            resultText.text = win ? "WIN" : "LOSE";
            timeText.text = ((float)parameter["time"]).ToString();
        }

        public void Restart()
        {
            App.levelManager.SetupScene();
            Hide();
        }

        public void ReturnToMenu()
        {
            App.gameManager.ReturnToMenu();
            Hide();
        }
    }
}
