using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Main.Behaviours
{
    public class GenericPlayer : MonoBehaviour
    {

        public UnityEvent<bool> onFinish = new UnityEvent<bool>();

        private Rigidbody2D rigidBody;
        private Quaternion originalRotation;

        protected virtual void Start()
        {
            originalRotation = transform.rotation;
        }

        protected Rigidbody2D rb
        {
            get
            {
                if(rigidBody == null)
                {
                    rigidBody = GetComponent<Rigidbody2D>();
                }
                return rigidBody;
            }
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            if(other.CompareTag("finish"))
            {
                onFinish.Invoke(true);
            }
        }

        void OnTriggerExit2D(Collider2D other)
        {
            if(other.CompareTag("GameArea"))
            {
                Logger.Log("game area leaved");
                onFinish.Invoke(false);
            }
        }

        public virtual void Reset(Vector3 pos)
        {
            transform.position = pos;
            transform.rotation = originalRotation;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = 0;
        }
    }
}
